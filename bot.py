import asyncio
import discord
import responses


# async function to await file to be sent to Discord before sending the rest of the message
async def send_message(message, user_message, is_private):
    try:
        # Get the bot's response and check if it's not the error message
        response = responses.get_response(user_message)
        if "Désolé" not in response:
            # Send the rolling_dice.gif file as a message
            with open("rolling_dice.gif", "rb") as f:
                picture = discord.File(f)
                gif_message = await message.channel.send(file=picture)

            # Wait for 2 seconds and delete the gif message
            await asyncio.sleep(2)
            await gif_message.delete()

        # Send the bot's response as a message
        await message.author.send(response) if is_private else await message.channel.send(response)

    except Exception as e:
        print(e)


def run_discord_bot():
    TOKEN = 'MTA4MTk0OTQ4NDAyNTY2MzYxOQ.GF0_NY.P24RcKmvDNAiEg3TgJfSdUy6C5xjsgf-QZFaVI'
    intents = discord.Intents.default()
    intents.message_content = True
    client = discord.Client(intents=intents)

    @client.event
    async def on_ready():
        print(f'{client.user} is now running ! ')

    @client.event
    async def on_message(message):
        if message.author == client.user:
            return

        username = str(message.author)
        user_message = str(message.content)
        channel = str(message.channel)

        print(f'{username} said : "{user_message}" ({channel})')

        if user_message[0] == '?':
            user_message = user_message[1:]
            await send_message(message, user_message, is_private=True)
        else:
            await send_message(message, user_message, is_private=False)

    client.run(TOKEN)
