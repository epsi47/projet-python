# Import necessary modules
import rolling  # for rolling dice

# Define color codes
GREEN = 0x00ff00  # hexadecimal color code for green color
RED = 0xff0000  # hexadecimal color code for red color


# This function will be called by the bot and will return a message based on the command given
def get_response(message: str) -> str:
    # call switch() function to determine the correct response to a given message
    return switch(message)


# This function takes a message string as input and returns a string
def switch(message):
    # convert message to lowercase
    p_message = message.lower()

    # check if message starts with '!roll'
    if p_message.startswith("!roll"):
        # check if '!roll' is followed by 2 integers separated by a space
        if len(p_message.split()) == 1 or not p_message.split()[1].isdigit():
            # if there are no or non-digit arguments, return an error message
            return handle_unknown()
        # if there are valid arguments, call handle_roll() function to roll the dice
        return handle_roll(message)

    # check if message starts with '!sortedroll'
    if p_message.startswith("!sortedroll"):
        # check if '!sortedroll' is followed by 2 integers separated by a space
        if len(p_message.split()) == 1 or not p_message.split()[1].isdigit():
            # if there are no or non-digit arguments, return an error message
            return handle_unknown()
        # if there are valid arguments, call handle_sortedroll() function to roll the dice and sort the results
        return handle_sortedroll(message)

    # check if message starts with '!statroll'
    if p_message.startswith("!statroll"):
        # check if '!statroll' is followed by 2 integers separated by a space
        if len(p_message.split()) == 1 or not p_message.split()[1].isdigit():
            # if there are no or non-digit arguments, return an error message
            return handle_unknown()
        # if there are valid arguments, call handle_statroll() function to roll the dice and generate statistical
        # results
        return handle_statroll(message)

    # check if message starts with '!help'
    if p_message.startswith("!help"):
        # if the message is a help request, call handle_help() function to show the available commands
        return handle_help()


# This function takes a message string as input, rolls the dice, and returns a formatted string
def handle_roll(message):
    # split the message into parts separated by spaces
    parts = message.split()

    # check if the message has 2 valid integers after '!roll' command
    if len(parts) == 3 and parts[1].isdigit() and parts[2].isdigit():
        # convert the 2nd and 3rd parts into integers
        nb_dices = int(parts[1])
        nb_faces = int(parts[2])
        # call roll() function from the rolling module to generate random numbers
        results = rolling.roll(nb_dices, nb_faces)

        # Format the results with the number of dice and faces rolled in bold green text
        formatted_results = f"Vous avez lancé {nb_dices}d{nb_faces} et obtenu :\n{results}"

        # Add a green color to the message
        return f"```diff\n+ {formatted_results}\n```"


# Function to handle sorted roll requests
def handle_sortedroll(message):
    # Split the message into parts
    parts = message.split()

    # Check if the message has the correct format
    if len(parts) == 3 and parts[1].isdigit() and parts[2].isdigit():
        # Convert the number of dice and faces to integers
        nb_dices = int(parts[1])
        nb_faces = int(parts[2])
        # Roll the dice using the rolling module and sort the results
        results = rolling.sortedroll(nb_dices, nb_faces)

        # Format the results with the number of dice and faces rolled in bold green text
        formatted_results = f"Vous avez lancé {nb_dices}d{nb_faces} et obtenu :\n{results}"

        # Add a green color to the message
        return f"```diff\n+ {formatted_results}\n```"


# Function to handle statroll requests
def handle_statroll(message):
    parts = message.split()

    # Check if the message has the correct format
    if len(parts) == 3 and parts[1].isdigit() and parts[2].isdigit():
        # Convert the number of dice and faces to integers
        nb_dices = int(parts[1])
        nb_faces = int(parts[2])
        # Roll the dice using the rolling module and sort the results
        dico = rolling.statroll(nb_dices, nb_faces)
        results = """
Lancés = {lancés}
Minimum = {min}
Maximum = {max}
Total = {total}
Moyenne = {moyenne}
Indice de chance = {chance}
""".format(lancés=dico['lancés'],
           min=dico['min'],
           max=dico['max'],
           total=dico['total'],
           moyenne=dico['moyenne'],
           chance=dico['indice de chance'])

        # Format the results with the number of dice and faces rolled in bold green text
        formatted_results = f"Vous avez lancé {nb_dices}d{nb_faces} et obtenu :\n{results}"

        # Add a green color to the message
        return f"```diff\n+ {formatted_results}\n```"


# Function to handle help requests
def handle_help():
    # Add a green color to the help message
    return f"```diff\n+ Vous pouvez lancer un dé en utilisant les commandes suivantes :\n  !roll <nombre de dés> <nombre de faces>\n  !sortedroll <nombre de dés> <nombre de faces>\n  !statroll <nombre de dés> <nombre de faces>\n```"


# Function to handle unkown requests
def handle_unknown():
    # If the message doesn't match any of the commands, add a red color to the error message
    return f"```diff\n- Désolé, je n'ai pas compris. Veuillez utiliser !help pour voir la liste des commandes disponibles.\n```"
