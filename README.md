#Ce projet Git contient le code source d'un bot Discord qui permet de lancer des dés virtuels et d'afficher les résultats dans un salon de discussion.

#Fonctionnalités
Le bot est capable de lancer des dés de différentes tailles (4, 6, 8, 10, 12, 20 et 100 faces) en utilisant la commande suivante :

!roll [nombre de dés]d[taille du dé]

Par exemple, pour lancer trois dés à six faces, il suffit d'envoyer la commande suivante dans le salon de discussion :
!roll 3d6

#Installation
Pour utiliser ce bot, vous devez d'abord créer un bot Discord et récupérer son token d'authentification.
Une fois que vous avez récupéré le token d'authentification de votre bot, clonez ce dépôt Git sur votre ordinateur :
https://gitlab.com/epsi47/projet-python.git

#Nécessaire
Utiliser cette  commande pour installer les dépendances du projet :
pip install -r requirement.txt
Puis exécuter le fichier main.py
