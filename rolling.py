import random

def roll(nb_dices, nb_faces) :
    results = []

    for i in range(nb_dices) :
        result = random.randint(1, nb_faces)
        results.append(result)
    return results

def sortedroll(nb_dices, nb_faces) :
    results = []

    for i in range(nb_dices) :
        result = random.randint(1, nb_faces)
        results.append(result)
        results = sorted(results, reverse=True)
    return results

def statroll(nb_dices, nb_faces) :
    roll = []

    for i in range(nb_dices) :
        result = random.randint(1, nb_faces)
        roll.append(result)
        results = sorted(roll, reverse=True)

    result = {}
    result["lancés"] = roll
    result["min"] = min(roll)
    result["max"] = max(roll)
    result["total"] = sum(roll)
    result["moyenne"] = sum(roll)/nb_dices
    result["indice de chance"] = sum(roll)/nb_dices/((nb_faces)/2)
    return result 
